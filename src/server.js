import express from 'express'
import cors from 'cors'
import bodyParser from 'body-parser'
import routes from './routes/routes'
import mongoose from 'mongoose'
import runtime from './database'
// Aux imports
import _log from './aux/underlog'

const PORT = 8088
const API = '/api'
const app = express()
// Enable CORS
app.use(cors())

// Database connection & error handling
mongoose.connect('mongodb://localhost:27017/npaw').catch(e => { _log(e) })

let db = mongoose.connection
db.on('error', error => { console.log(`Connection error: ${error}`) })
db.once('open', runtime)

// API routes
app.get(`${API}/hello`, routes.hello)

app.get(`${API}/accounts/:id?`, routes.accounts.get)
app.post(`${API}/accounts`, bodyParser.json(), routes.accounts.post)
app.put(`${API}/accounts/:id`, bodyParser.json(), routes.accounts.put)
app.delete(`${API}/accounts/:id`, routes.accounts.del)

app.get(`${API}/permissions/:id?`, routes.permissions.get)
app.post(`${API}/permissions`, bodyParser.json(), routes.permissions.post)
app.put(`${API}/permissions/:id`, bodyParser.json(), routes.permissions.put)
app.delete(`${API}/permissions/:id`, routes.permissions.del)

// Start listener on ${PORT}
app.listen(PORT, () => {
  console.log(`[Server] Magic happening at ${PORT}`)
})

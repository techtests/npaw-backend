import get from './get'
import post from './post'
import put from './put'
import del from './del'

export default {
  get,
  post,
  put,
  del
}

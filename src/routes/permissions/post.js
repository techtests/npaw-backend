import Permission from '../../models/permission.model'
import _log from '../../aux/underlog'

export default (req, res) => {
  if (!req.body.permission) {
    res.status(400).send('Expecting a nice JSON object, got shit >:(')
  } else {
    _log('[Permissions] POST: Someone posted a new permission')

    let newPermission = new Permission(req.body.permission)

    newPermission.save().then(
      permission => { res.status(201).send(permission) },
      error => { res.status(500).send(error) }
    )
  }
}

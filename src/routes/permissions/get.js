import Permission from '../../models/permission.model'
import _log from '../../aux/underlog'

export default (req, res) => {
  if (req.params.id) {
    _log(`[Permissions] GET: Someone requested id: ${req.params.id}`)
    Permission.findOne({_id: req.params.id}, (error, permission) => {
      if (error) {
        res.status(500).send(`Error: ${error}`)
      }

      if (!permission) {
        _log(`[Permissions] List is empty`)
        res.status(204).send(permission)
      } else {
        res.send(permission)
      }
    })
  } else {
    _log(`[Permissions] GET: Someone requested all permissions`)

    Permission.find((error, permissions) => {
      if (error) {
        res.status(500).send(`Error: ${error}`)
      }

      if (permissions.length === 0) {
        _log(`[Permissions] List is empty`)
        res.status(204).send(permissions)
      } else {
        res.send(permissions)
      }
    })
  }
}

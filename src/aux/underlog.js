const DEBUG = true
export default message => {
  if (DEBUG) {
    console.log(message)
  }
}
